import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from './Home_Page/Home';
import './App.css';
import Singup from './Home_Page/Signup';
import Login from './Home_Page/Login';
function App() {
  return (
    <BrowserRouter>
    <div className="App">
       <Home />
      <Switch>
         <Route path="/signup"component={Singup}/>
         <Route path="/login"component={Login}/>
      </Switch>
      
    </div>
  </BrowserRouter>
  );
}

export default App;
